import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Signup from '@/components/Signup'
import ForgotPassword from '@/components/ForgotPassword'
import MainContainer from '@/components/MainContainer'
import NewPost from '@/components/NewPost'
import AccountSetting from '@/components/AccountSetting'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/forgot',
      name: 'ForgotPassword',
      component: ForgotPassword
    },
    {
      path: '/post',
      alias: ['/feed', '/'],
      name: 'MainContainer',
      component: MainContainer
    },
    {
      path: '/newpost',
      name: 'NewPost',
      component: NewPost
    },
    {
      path: '/setting',
      name: 'AccountSetting',
      component: AccountSetting
    }
  ]
})
